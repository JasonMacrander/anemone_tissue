## This is a repository for data and code in association with the publication submitted to Genome Biology and Evoluiton:

**Macrander, J., Broe, M., Daly, M. 2016. Tissue-Specific Venom Composition and Differential Gene Expression in Sea Anemones. Genome Biology and Evolution, *In Prep* **